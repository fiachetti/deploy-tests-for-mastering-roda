#+SETUPFILE: ../defaults.org

This is a plugin that I really love. Not only because it's very useful, but because it's a perfect example of the power and the simplicity that /Roda/ brings to the table.

Say we have an application that displays a cinema's billboard.

We have our list of movies. Each one includes it's title, next show time for today and a description.

In order to make quick objects from this, we use =OpenStructs=.

#+begin_src ruby
  require "roda"
  require "awesome_print"
  require "ostruct"

  class App < Roda
    plugin :empty_root

    route do |r|

      movies = [
        {
          slug: "infinity-war",
          title: "Avengers Infinity War",
          times: ["15:30", "18:40", "21:45"],
          description: "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."
        },
        {
          slug: "the-usual-suspects",
          title: "The Usual Suspects",
          times: ["11:10", "15:45"],
          description: "A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which began when five criminals met at a seemingly random police lineup."
        },
        {
          slug: "the-matrix",
          title: "The Matrix",
          times: ["17:15", "22:10"],
          description: "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers."
        },
      ].map {|movie_hash| OpenStruct.new(movie_hash)}

      # ...
    end
  end
#+end_src

And for our routes, we have a =/movies= path for listing our movies. Each one displays the movie title and a url.

We also have a =/movies/<slug>= path for displaying a specific movie. Here we show the title, times and description for.

#+begin_src ruby
  class App < Roda
    route do |r|
      r.on "movies" do
        r.root  do
          movies.map { |movie|
            "#{movie.title}: /movies/#{movie.slug}"
          }.join("\n")
        end

        r.on :slug do |slug|
          movie = movies.find { |m| m[:slug] == slug }

          <<~EOF
            #{movie.title}
            Times: [ #{movie.times.join(" ")} ]
            Description: #{movie.description}
          EOF
        end
      end
    end
  end
#+end_src

Once our cinema app has some traction, we decide to create a mobile app. So we need a JSON API for it to consume.

For simplicity, lets make some changes to the current app in order to make it return some JSON.

We first convert the index action to a JSON string. In order to do this, we start with an opening square bracket. Then, for each movie, we need to return the JSON string with it's title and url. And join all of them with a comma. Finally we add a closing square bracket.

We also need to set the content type header of the response to let the client know that we're returning JSON

#+begin_src ruby
  # ...

  r.root  do
    response['Content-Type'] = 'application/json'

    "[" +
      movies.map { |movie|
      "{ \"title\": \"#{movie.title}\", \"url\": \"/movies/#{movie.slug}\" }"
    }.join(", ") +
      "]"
  end

  # ...
#+end_src

And now, when we take a look at the end result, we see our JSON response

#+begin_src ruby
  require "lucid_http"
  require "json"

  GET "/movies", json: true
  # => [{"title"=>"Avengers Infinity War", "url"=>"/movies/infinity-war"},
  #     {"title"=>"The Usual Suspects", "url"=>"/movies/the-usual-suspects"},
  #     {"title"=>"The Matrix", "url"=>"/movies/the-matrix"}]
#+end_src

But this is hardly the best way to do this. Instead, we can require =json= from the standard library and let it do all the heavy lifting.

We replace all the ugly string concatenation and interpolation with an array of hashes and call =to_json= on it.

#+begin_src ruby
  r.root  do
    response['Content-Type'] = 'application/json'

    movies.map {|movie| {title: movie.title, url: "/movies/#{movie.slug}"}}.to_json
  end
#+end_src

And everything still works as before.

#+begin_src ruby
  require "lucid_http"
  require "json"

  GET "/movies", json: true
  # => [{"title"=>"Avengers Infinity War", "url"=>"/movies/infinity-war"},
  #     {"title"=>"The Usual Suspects", "url"=>"/movies/the-usual-suspects"},
  #     {"title"=>"The Matrix", "url"=>"/movies/the-matrix"}]
#+end_src

Now we move on to the routes for each specific movie.

Here we change the previous string to a simple hash instead of an array.

The title and description are pretty straight forward changes. For the times attribute, we can actually simplify it a lot, since we now need only an array.

And, since we need to set the content type, instead of duplicating the code, we move it to the wrapping block.

#+begin_src ruby
  # ...

  r.on "movies" do
    response['Content-Type'] = 'application/json'

    r.root  do
      movies.map {|movie| {title: movie.title, url: "/movies/#{movie.slug}"}}.to_json
    end

    r.on :slug do |slug|
      movie = movies.find { |m| m[:slug] == slug }

      {
        title:       movie.title,
        times:       movie.times,
        description: movie.description
      }
    end

  end

  # ...
#+end_src

Lets try this out. It blows up! Why? When we look at the error, we see that we're returning a hash, which means that we forgot to call =to_json= on our hash.

#+begin_src ruby
  require "lucid_http"
  require "json"

  GET "/movies/infinity-war", json: true
  # => "STATUS: 500 Internal Server Error"
  error

  # ~> JSON::ParserError
  # ~> 765: unexpected token at 'Roda::RodaError: unsupported block result: {:title=>"Avengers Infinity War", :times=>["15:30", "18:40", "21:45"], :description=>"The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."}
  # ~> \t/home/fedex/.rvm/gems/ruby-2.5.1/gems/roda-3.16.0/lib/roda.rb:803:in `block_result_body'
  # ~> \t/home/fedex/.rvm/gems/ruby-2.5.1/gems/roda-3.16.0/lib/roda.rb:393:in `block_result'
  # ~> ...
#+end_src

Once we do it,

#+begin_src ruby
  # ...

  r.on :slug do |slug|
    movie = movies.find { |m| m[:slug] == slug }

    {
      title:       movie.title,
      times:       movie.times,
      description: movie.description
    }.to_json
  end

  # ...
#+end_src

everything starts working.

#+begin_src ruby
  require "lucid_http"
  require "json"

  GET "/movies/infinity-war", json: true
  # => {"title"=>"Avengers Infinity War",
  #     "times"=>["15:30", "18:40", "21:45"],
  #     "description"=>
  #      "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."}
#+end_src

This approach has 2 inconveniences. First, we need to set the proper header in order to be good citizens. And second, we run the risk of forgetting to add =.to_json= to the end of every result (and we're not gonna go back to interpolating strings!).

To mitigate both this inconveniences, /Roda/ ships with the =json= plugin.

To use it, we just need to add it to our app. And then, every time we return an array or a hash, the plugin will take care of setting the correct headers and returning the correct response.

Also, we don't need to import the =json= library any more, since it's being imported by the plugin.

#+begin_src ruby
  class App < Roda
    plugin :empty_root
    plugin :json

    route do |r|

      movies = [
        {
          slug: "infinity-war",
          title: "Avengers Infinity War",
          times: ["15:30", "18:40", "21:45"],
          description: "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."
        },
        {
          slug: "the-usual-suspects",
          title: "The Usual Suspects",
          times: ["11:10", "15:45"],
          description: "A sole survivor tells of the twisty events leading up to a horrific gun battle on a boat, which began when five criminals met at a seemingly random police lineup."
        },
        {
          slug: "the-matrix",
          title: "The Matrix",
          times: ["17:15", "22:10"],
          description: "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers."
        },
      ].map {|movie_hash| OpenStruct.new(movie_hash)}

      r.on "movies" do
        r.root  do
          movies.map {|movie| {title: movie.title, url: "/movies/#{movie.slug}"}}
        end

        r.on :slug do |slug|
          movie = movies.find { |m| m[:slug] == slug }

          {
            title:       movie.title,
            times:       movie.times,
            description: movie.description
          }
        end

      end

    end
  end
#+end_src

Lets try it one more time. And everything works.

#+begin_src ruby
  require "lucid_http"
  require "json"

  GET "/movies", json: true
  # => [{"title"=>"Avengers Infinity War", "url"=>"/movies/infinity-war"},
  #     {"title"=>"The Usual Suspects", "url"=>"/movies/the-usual-suspects"},
  #     {"title"=>"The Matrix", "url"=>"/movies/the-matrix"}]

  GET "/movies/infinity-war", json: true
  # => {"title"=>"Avengers Infinity War",
  #     "times"=>["15:30", "18:40", "21:45"],
  #     "description"=>
  #      "The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe."}
#+end_src

Remember way back on the basic routing lesson that we said that /Roda/ interprets anything other than a string as a miss?, well now we can see that behavior can be overwritten by using plugins such as this one, which is a very big win. It also comes back to what I said at the beginning of this lesson, plugins like this one shows the power and simplicity of /Roda/!
