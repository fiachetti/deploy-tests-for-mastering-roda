#+SETUPFILE: ../defaults.org

#+INCLUDE: ../defaults.org

/Roda/ has a philosophy of being un-opinionated out of the box. If you followed along up to this point, you already know probably 80% of what we could call "Vanilla /Roda/". We could write a whole app with what we know at the moment.

But there's more to it. /Roda/ also comes with a very complete and extensive set of plugins that you won't wanna miss. This lesson will be an exploration of the plugin system. We'll learn how to browse, and use plugins.

In the subsequent sections we'll explore some of what I think are the most useful plugins that come with /Roda/.

To find the list of plugins, we can go to the /Roda/ site at [[http://roda.jeremyevans.net/]] and click on the /Documentation/ link.  Here we can find a list of /Plugins/. Clicking on any of the plugins listings will get us detailed information on that plugin.

Lets try one out. We'll go with the =public= plugin.

On the top of it's documentation page, we see a title and a link pointing to the actual file's documentation.

On the last section, we see a link to all the classes and modules the plugin modifies and the constants and public class methods it defines.

On the body we see a short description of what the plugin actually does. And an example showing us how it should be used.

If we decide to go and actually use one of this plugins and we find ourselves caught on an edge case, or if we want to know how it is written just for fun, we can find the source code for any of them in /Roda/'s =Github= page ([[https://github.com/jeremyevans/roda]]) under the =lib/roda/plugins/<plugin name>.rb= file.

Lets try this plugin out, so we can see it in action. We have an app without any routes.

#+begin_src ruby
  require "roda"
  require "awesome_print"

  class App < Roda
    route do

    end
  end
#+end_src

Of course, right now, any request we make will result in a =404 Not Found= status. We try with http://localhost:9292/dave.html because reasons and that's exactly what we get.

#+begin_src ruby
  require "lucid_http"

  GET "/dave.html"
  status                          # => "404 Not Found"
#+end_src

Now, lets add the plugin. To add any plugin, we need to invoke the =plugin= class method inside our app class and pass the plugin name as a symbol.

With just that, the plugin is in place. This is how we add any plugin on the list.

With just that, the plugin is in place. Notice that we didn't have to add any new gems to our project, since this is one of /Roda/'s built-in plugins. We also didn't need to add a =require= statement. /Roda/ arranges to load the needed code based on the name of the plugin.

With the plugin added to our app, we can immediately start using it.

The =public= plugin adds a helper method called =public= to the request. When we invoke it inside the =route= block, it will add a route that will allow us to serve static files inside a specific directory.

#+begin_src ruby
  require "roda"
  require "awesome_print"

  class App < Roda
    plugin :public

    route do |r|
      r.public
    end
  end
#+end_src

By default, it will serve files from the directory =<app dir>/public=. Lets create it and add some files to it

#+begin_src ruby
  Dir.mkdir("./pulic")

  %w{chris dave matt pete}.each_with_index do |doc_name, i|
    doc_num = (i+9).to_s
    File.open("#{doc_name}.html", "w") do |f|
      content = "<h2>My name is #{doc_name.capitalize} <h2>\n<h3>and I'm number #{doc_num}</h3>\n"
      f.write(content)
    end
  end
#+end_src

Now that we have something there,

#+begin_example
  $ ls public/
  chris.html  dave.html  matt.html  pete.html
#+end_example

we browse again to http://localhost:9292/dave.html, and we see that now we get a rendered page and a =200 OK= status.

#+begin_src ruby
  require "lucid_http"

  GET "/dave.html"
  body               # => "<h2>My name is Dave <h2>\n<h3>and I'm number 10</h3>\n"
  status             # => "200 OK"
#+end_src

Now, say we don't want to serve files out of the =public= directory. We want to serve files from a directory called =static= instead.

To do that, we need to configure the plugin. We pass a named parameter called =root= , with =static= as the value.

#+begin_src ruby
  require "roda"
  require "awesome_print"

  class App < Roda
    plugin :public, root: "static"

    route do |r|
      r.public
    end
  end
#+end_src

Now, when the app is restarted, the configuration for the plugin changed. Instead of looking into the =public= directory, it will look into =static=. Of course, when we try this, it'll return a =404 Not Found= status because the directory doesn't exist yet.

#+begin_src ruby
  require "lucid_http"

  GET "/dave.html"
  body                          # => ""
  status                        # => "404 Not Found"
#+end_src

But if we rename the =public= directory to =static=,

#+begin_example
  mv public static
#+end_example

and retry, it now works again.

#+begin_src ruby
  require "lucid_http"

  GET "/dave.html"
  body               # => "<h2>My name is Dave <h2>\n<h3>and I'm number 10</h3>\n"
  status             # => "200 OK"

  GET "/pete.html"
  body               # => "<h2>My name is Pete <h2>\n<h3>and I'm number 12</h3>\n"
  status             # => "200 OK"
#+end_src

This is the common pattern for customizing plugin configuration: we pass named parameters after specifying the name of the plugin.

We've learned how to browse, use and configure plugins. In the next sections, we'll start exploring the plugins library that come with /Roda/. We'll go in depth into each one of them.
