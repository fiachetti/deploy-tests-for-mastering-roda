Mastering Roda
==============

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Mastering Roda</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/fiachetti/mastering-roda" property="cc:attributionName" rel="cc:attributionURL">Federico Iachetti</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/fiachetti/mastering-roda" rel="dct:source">https://gitlab.com/fiachetti/mastering-roda</a>.


# Building the book

If you're contributing to the project and want to build the book after some changes, you first need to have a current version of Pandoc installed on your system. I'm currently using version 2.7.3.

Once you have Pandoc, you can run the following command (make sure to be standing in the root directory of the project):

```
  pandoc -f org -t html5 -o ~/books/mastering-roda/public/index.html --standalone ~/books/mastering-roda/mastering-roda.org --css ./css/pandoc.css --toc --toc-depth=3
```
    
